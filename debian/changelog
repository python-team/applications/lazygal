lazygal (0.10.11-1) unstable; urgency=medium

  * New upstream version 0.10.11 (Closes: #1092599)
  * declare compliance to policy 4.7.2.0 (no change)

 -- Alexandre Rossi <niol@zincube.net>  Fri, 28 Feb 2025 10:40:38 +0100

lazygal (0.10.10-2) unstable; urgency=medium

  * Team upload.
  * Set the maintainer to the team to replace the retired package maintainer.
    Thanks to Michal Čihař for their past work!
  * debian/control: Use dh-sequence-python3.

 -- Boyuan Yang <byang@debian.org>  Wed, 23 Oct 2024 12:08:21 -0400

lazygal (0.10.10-1) unstable; urgency=medium

  * New upstream version 0.10.10 (Closes: #1077438, #1008965)

 -- Alexandre Rossi <niol@zincube.net>  Mon, 29 Jul 2024 11:41:13 +0200

lazygal (0.10.9-1) unstable; urgency=medium

  * New upstream version 0.10.9 (Closes: #1061807)

 -- Alexandre Rossi <niol@zincube.net>  Wed, 31 Jan 2024 05:59:04 +0100

lazygal (0.10.8-1) unstable; urgency=medium

  * properly clean build artifacts (Closes: #1046272)
  * New upstream version 0.10.8 (Closes: #1055818)

 -- Alexandre Rossi <niol@zincube.net>  Sun, 12 Nov 2023 11:14:35 +0100

lazygal (0.10.7-1) unstable; urgency=medium

  * New upstream version 0.10.7 (Closes: #1040632)

 -- Alexandre Rossi <niol@zincube.net>  Sun, 09 Jul 2023 13:01:00 +0200

lazygal (0.10.6-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

  [ Alexandre Rossi ]
  * New upstream version 0.10.6
  * update maintainer email

 -- Alexandre Rossi <niol@zincube.net>  Tue, 20 Jun 2023 15:18:42 +0200

lazygal (0.10.5-1) unstable; urgency=medium

  * New upstream version 0.10.5
  * update dep to setuptools (Closes: #1004983)

 -- Alexandre Rossi <alexandre.rossi@gmail.com>  Sat, 05 Mar 2022 18:06:49 +0100

lazygal (0.10.3-2) unstable; urgency=medium

  * add d/upstream/metadata

 -- Alexandre Rossi <alexandre.rossi@gmail.com>  Mon, 06 Dec 2021 23:18:28 +0100

lazygal (0.10.3-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Alexandre Rossi ]
  * New upstream version 0.10.3
  * clean built manpages
  * set standards version to 4.6.0 (no change)

 -- Alexandre Rossi <alexandre.rossi@gmail.com>  Mon, 08 Nov 2021 06:37:47 +0100

lazygal (0.10.2-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Alexandre Rossi ]
  * New upstream version 0.10.2
  * d/watch: update version to  4
  * update to standards version 4.5.1 (no change)

 -- Alexandre Rossi <alexandre.rossi@gmail.com>  Sun, 07 Feb 2021 19:40:58 +0000

lazygal (0.10.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Alexandre Rossi ]
  * New upstream version 0.10.1
  * drop: autopkgtest: allow stderr for now

 -- Alexandre Rossi <alexandre.rossi@gmail.com>  Wed, 14 Oct 2020 18:38:17 +0200

lazygal (0.10-1) unstable; urgency=medium

  * New upstream version 0.10
  * depend on ffmpeg instead of gst
  * fix missing-license-paragraph-in-dep5-copyright
  * autopkgtest: allow stderr for now

 -- Alexandre Rossi <alexandre.rossi@gmail.com>  Thu, 27 Aug 2020 13:43:12 +0200

lazygal (0.9.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Alexandre Rossi ]
  * New upstream version 0.9.4
  * New deps (pandoc to build manpages and jquery)
  * Machine-readable debian/copyright
  * Raise dh compat level to 13
  * raise policy compliance to 4.5.0 (nothing to do)
  * drop useless versionned dependency on python
  * add Rules-Requires-Root
  * autopkgtest: try a real album generation

 -- Alexandre Rossi <alexandre.rossi@gmail.com>  Mon, 18 May 2020 15:16:20 +0200

lazygal (0.9.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field

  [ Alexandre Rossi ]
  * New upstream version 0.9.3 (Closes: #902764)
  * add basic autopkgtest

 -- Alexandre Rossi <alexandre.rossi@gmail.com>  Wed, 10 Oct 2018 22:38:17 +0200

lazygal (0.9.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces

  [ Alexandre Rossi ]
  * New upstream version 0.9.2
  * build-depend on dh-python (Closes: #896730)
  * update upstream homepage
  * build depend on python3-distutils

 -- Michal Čihař <nijel@debian.org>  Fri, 04 May 2018 11:00:37 +0200

lazygal (0.9.1-1) unstable; urgency=medium

  * New upstream release.
  * Use Python 3 (Closes: #843971).

 -- Michal Čihař <nijel@debian.org>  Fri, 18 Nov 2016 17:57:55 +0100

lazygal (0.9-1) unstable; urgency=medium

  * New upstream release.
    - Fixes crash with missing python-gst-1.0 (Closes: #829447).
    - Fixes sorting by timestamp (Closes: #794899).
    - Fixes crash withou input files (Closes: #776198).
    - Fixes inverted theme padding (Closes: #782376).
    - Do not warn about missing video support when no videos are
      found (Closes: #836697).
  * Bump standards to 3.9.8.

 -- Michal Čihař <nijel@debian.org>  Thu, 10 Nov 2016 13:30:19 +0100

lazygal (0.8.8-2) unstable; urgency=medium

  * Fixed gstreamer dependencies.

 -- Michal Čihař <nijel@debian.org>  Wed, 29 Oct 2014 10:29:43 +0100

lazygal (0.8.8-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Wed, 22 Oct 2014 14:59:16 +0200

lazygal (0.8.7-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Mon, 06 Oct 2014 08:50:04 +0200

lazygal (0.8.6-1) unstable; urgency=medium

  * New upstream release.
    - Fixes index out of range error (Closes: #760780).
  * Bump standards to 3.9.6.

 -- Michal Čihař <nijel@debian.org>  Mon, 22 Sep 2014 11:03:44 +0200

lazygal (0.8.4-1) unstable; urgency=medium

  * New upstream release.
    - Fix semicolon not escaped in urls (Closes: #745979).
  * Depend on gir1.2-gexiv2-0.10 0.10.1 or newer to avoid breakage with 0.10.0
    (Closes: #745976).

 -- Michal Čihař <nijel@debian.org>  Tue, 13 May 2014 12:11:03 +0200

lazygal (0.8.3-1) unstable; urgency=low

  * New upstream release.
  * Update upstream homepage.

 -- Michal Čihař <nijel@debian.org>  Fri, 11 Apr 2014 14:16:52 +0200

lazygal (0.8.2-2) unstable; urgency=low

  * Adjust Vcs-Svn URL.
  * Update exiv2 dependency to gir1.2-gexiv2-0.10 (Closes: #742804).

 -- Michal Čihař <nijel@debian.org>  Tue, 01 Apr 2014 11:10:53 +0200

lazygal (0.8.2-1) unstable; urgency=low

  * Bump standards to 3.9.5.
  * New upstream release.
    - Includes source for jQuery (Closes: #736736).

 -- Michal Čihař <nijel@debian.org>  Mon, 24 Feb 2014 11:10:31 +0100

lazygal (0.8.1-1) unstable; urgency=low

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Thu, 31 Oct 2013 09:06:57 +0100

lazygal (0.8-1) unstable; urgency=low

  [ Michal Čihař ]
  * New upstream release.
    - Uses new Genshi templates (Closes: #696682).
    - Correctly handles wronly encoded artist in EXIF (Closes: #696648).
    - Uses GExiv2 (LP: #1074028).
  * Depend on GExiv2 instead of pyexiv2.
  * Bump standards to 3.9.4.
  * Use debhelper 9.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Michal Čihař <nijel@debian.org>  Thu, 06 Jun 2013 12:05:08 +0200

lazygal (0.7.4-1) unstable; urgency=low

  * New upstream release.
    - Brings back missing default config (Closes: #681724).

 -- Michal Čihař <nijel@debian.org>  Tue, 17 Jul 2012 10:51:46 +0200

lazygal (0.7.3-1) unstable; urgency=low

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Fri, 29 Jun 2012 20:50:07 +0200

lazygal (0.7.2-1) unstable; urgency=low

  * New upstream release.
    - Fix video thumbnailing handling (Closes: #662118).
    - Fixes French translation (Closes: #664167).
  * Clean generated files in clean phase (Closes: #671317).
  * Bump standards to 3.9.3.

 -- Michal Čihař <nijel@debian.org>  Mon, 14 May 2012 10:26:46 +0200

lazygal (0.7.1-1) unstable; urgency=low

  * New upstream release.
    - Patch was merged upstream.

 -- Michal Čihař <nijel@debian.org>  Wed, 30 Nov 2011 09:24:25 +0100

lazygal (0.7-1) unstable; urgency=low

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Wed, 16 Nov 2011 13:18:57 +0100

lazygal (0.6.2-1) unstable; urgency=low

  * New upstream release.
    - fix failure when one use -O and there is a video (Closes: #631181)
    - fix failure when date tags are not recognized by pyexiv2
      (Closes: #630572)

 -- Michal Čihař <nijel@debian.org>  Fri, 05 Aug 2011 13:32:23 +0200

lazygal (0.6.1-1) unstable; urgency=low

  * New upstream release.
  * Bump standards to 3.9.2.

 -- Michal Čihař <nijel@debian.org>  Mon, 02 May 2011 09:38:33 +0200

lazygal (0.6-2) unstable; urgency=low

  * Update home page.
  * Suggest gstreamer for video support.
  * Require python 2.6 or newer (Closes: #619032).

 -- Michal Čihař <nijel@debian.org>  Mon, 21 Mar 2011 15:55:53 +0100

lazygal (0.6-1) unstable; urgency=low

  * New upstream release.
  * Switch to dh_python2.

 -- Michal Čihař <nijel@debian.org>  Wed, 09 Mar 2011 13:41:36 +0100

lazygal (0.5.1-1) unstable; urgency=low

  * New upstream version.
    - Fixes problems with PyExiv 0.2.
    - Updates translations.
    - Fixed image-index theme.
  * Bump standards to 3.9.1.

 -- Michal Čihař <nijel@debian.org>  Thu, 26 Aug 2010 14:28:54 +0200

lazygal (0.5-2) unstable; urgency=low

  * Lazygal does not work with Python 2.4 (Closes: #588262).
  * Bump standards to 3.9.0.

 -- Michal Čihař <nijel@debian.org>  Wed, 07 Jul 2010 10:44:17 +0200

lazygal (0.5-1) unstable; urgency=low

  * New upstream release.
    - Supports flattening of directory structure (Closes: #522223).
    - Our single patch was integrated upstream.

 -- Michal Čihař <nijel@debian.org>  Tue, 15 Jun 2010 14:31:51 +0200

lazygal (0.4.1-3) unstable; urgency=low

  * Build depend on python instead of python-dev.
  * Drop celementtree dependency, use python >= 2.5 instead.
  * Bump standards to 3.8.4.

 -- Michal Čihař <nijel@debian.org>  Fri, 05 Mar 2010 10:04:40 +0100

lazygal (0.4.1-2) unstable; urgency=low

  * Convert to 3.0 (quilt) source format.
  * Simplify debian/rules.
  * Bump standards to 3.8.3.

 -- Michal Čihař <nijel@debian.org>  Mon, 23 Nov 2009 09:35:50 +0100

lazygal (0.4.1-1) unstable; urgency=low

  * New upstream version.
    - Fix "handling broken for directory names containing period" (Closes:
      #504040).
    - Fix "order of pictures in a folder?" (Closes: #495503).
    - Fix "resizing parameters not flexible enough" (Closes: #493541).
    - Fix "link originals without copying" (Closes: #504039).
    - Fix "empty placeholder image in menu if directory has no images"
      (Closes: #504041).
  * Bump standards to 3.8.1.
  * Drop debian man page, it has been integrated upstream.
  * Build depend on docbook-xsl, xsltproc for generating man page.
  * Add patch to fix upstream man page installation.

 -- Michal Čihař <nijel@debian.org>  Thu, 21 May 2009 15:18:52 +0200

lazygal (0.4-2) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Marco Rodrigues ]
  * debian/control:
    + Add ${misc:Depends} to Depends to remove
      lintian warning.

  [ Michal Čihař ]
  * Switch to python-support (in fact the switch did happen unintentionally in
    0.3.1-3, but build deps reflect it now).
  * Adjust description to match current policy (lower case).
  * Update debian/copyright (year, GPL symlink).

 -- Michal Čihař <nijel@debian.org>  Sun, 15 Feb 2009 14:18:10 +0100

lazygal (0.4-1) unstable; urgency=low

  * New upstream version.
    - Fixes handling of comments with \0 (Closes: #485286).

 -- Michal Čihař <nijel@debian.org>  Sun, 06 Jul 2008 22:54:38 +0200

lazygal (0.3.1-3) unstable; urgency=low

  * Move packaging to Python Applications Packaging Team:
    - Change Vcs fields in debian/control.
    - Add team to Uploaders.
  * Update to standards 3.8.0.
  * Use new dh command to simplify debian/rules.

 -- Michal Čihař <nijel@debian.org>  Sat, 07 Jun 2008 12:01:19 +0200

lazygal (0.3.1-2) unstable; urgency=low

  * Allow installation with python 2.5 where celementtree is merged inside
    Python (Closes: #476462).

 -- Michal Čihař <nijel@debian.org>  Wed, 16 Apr 2008 22:01:37 +0200

lazygal (0.3.1-1) unstable; urgency=low

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Tue, 01 Apr 2008 10:19:42 +0200

lazygal (0.3-2) unstable; urgency=low

  * Drop workaround for bug #452227 (Closes: #472011).

 -- Michal Čihař <nijel@debian.org>  Mon, 24 Mar 2008 20:57:18 +0100

lazygal (0.3-1) unstable; urgency=low

  * New upstream release.
    - PyExiv2 patch no longer needed.

 -- Michal Čihař <nijel@debian.org>  Tue, 11 Mar 2008 09:40:34 +0100

lazygal (0.2-3) unstable; urgency=low

  * Adjusted Vcs-* headers to point to trunk.
  * Fixed compatibility with current PyExiv2.

 -- Michal Čihař <nijel@debian.org>  Tue, 26 Feb 2008 11:29:24 +0100

lazygal (0.2-2) unstable; urgency=low

  * Delete empty /usr/lib (workaround for pycentral bug #452227).

 -- Michal Čihař <nijel@debian.org>  Fri, 25 Jan 2008 11:43:22 +0900

lazygal (0.2-1) unstable; urgency=low

  * New upstream version.
    - Depends on python-elementtree.
  * Update policy to 3.7.3 (no changes needed).
  * No need to patch anymore.

 -- Michal Čihař <nijel@debian.org>  Fri, 25 Jan 2008 09:47:01 +0900

lazygal (0.1-2) unstable; urgency=low

  * Brown paper bag upload.
  * Do not use description and dependencies from photo-uploader, use the ones
    appropriate for this package (Closes: #451544).

 -- Michal Čihař <nijel@debian.org>  Sat, 17 Nov 2007 10:57:42 +0900

lazygal (0.1-1) unstable; urgency=low

  * Initial release (Closes: #445008).

 -- Michal Čihař <nijel@debian.org>  Mon, 12 Nov 2007 17:26:01 +0900
